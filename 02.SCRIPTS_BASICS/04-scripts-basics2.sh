#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció: Processar stdin mostrant per stdout les línies numerades i en majúscules.
#	
#	$prog stdin.upper
# ------------------------------

# Numera i normalitza a majuscules la entrada stdin.

num=1

while read -r line
do
  echo "$num. $line" | tr '[:lower:]' '[:upper:]'
  ((num++))
done
