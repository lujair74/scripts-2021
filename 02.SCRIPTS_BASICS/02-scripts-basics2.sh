#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció: Processar els arguments i comptar quantes n’hi ha de 3 o més caràcters.
#
#		- Validar que n'hi ha un argument mínim.
#		- I comptar quants hi ha de 3 o més caràcters
#	
#	$prog arg3+
# ------------------------------

ERR_NOARGS=1

# Validar nº d'arguments

if [ $# -eq 0 ]; then
  echo "ERR: Número args incorrecte"
  echo "Usage: $0 no args"
  exit $ERR_NOARGS
fi

# Comptar quants n'hi ha de 3 o més caràcters

num=0

for args in $*
do

  echo $args | egrep '.{3}' &> /dev/null
  if [ $? -eq 0 ]; then 
    num=$((num+1))
  fi

done
echo "N'hi ha $num arguments de 3 o més caràcters"
exit 0
