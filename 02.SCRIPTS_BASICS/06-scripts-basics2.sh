#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció: Processar per stdin linies d’entrada tipus “Tom Snyder” i mostrar per stdout la línia en format → T. Snyder..
#	
#	$prog n.cognom
# ------------------------------

# Processa l'entrada stdin amb el format correcte.

while read -r line
do
  nom=$(echo $line | cut -c1)
  cognom=$(echo $line | cut -d' ' -f2)
  echo "$nom. $cognom"
done
exit 0

