#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció:  Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
#
#		- Validar que n'hi ha un argument mínim.
#		- Mostrar només el de 4 o més caràcters.
#	
#	$prog arg4+
# ------------------------------
m 
ERR_NOARGS=1

# Validar nº d'arguments

if [ $# -eq 0 ]; then
  echo "ERR: Número args incorrecte"
  echo "Usage: $0 no args"
  exit $ERR_NOARGS
fi

# Comptar quants n'hi ha de 3 o més caràcters

num=0

for args in $*
do

  echo $args | egrep '.{4}' &> /dev/null
  if [ $? -eq 0 ]; then 
    echo $args
  fi

done
exit 0
