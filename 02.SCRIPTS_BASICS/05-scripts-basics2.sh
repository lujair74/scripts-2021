#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció: Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
#
#		- Mostrar les línies de menys de 50 caràcters.
#	
#	$prog c50
# -----------------------------

# Mostrar les lines de menys de 50 caràcters

while read -r line
do
  for car in line
  do

    echo $car | egrep '.{4}' &> /dev/null
    if [ $? -ne 0 ]; then 
      echo $car
    fi
  done

done

