#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció:
#
#
# 	- Validar existeix almenys un file. Per a cada file comprimir-lo. Generar per stdout el nom del file comprimit si s’ha comprimit correctament, o un missatge d’error per stderror si no s’ha pogut comprimir. En finalitzar es mostra per stdout quants files ha comprimit. 
#
#	- Retorna status 0 ok, 1 error nº args, 2 si algun error en comprimir.
#
#	- Ampliar amb el cas: prog -h|--help.
#
#    $prog file...
# -----------------------------------------------

ERR_NARGS=1

# Validem arguments.

if [ $# -eq 0 ]; then
  echo "Error: número d'arguments incorrecte."
  echo "Usage: $0 file..."
  exit $ERR_NARGS
fi

# Si el programa crida l'opció "-h" o "--help".

if [ $1 = "-h" -o $1 = "--help" ]; then
  echo "H1SX 21-22 Escola Del Treball. -> Lucas Rodríguez Cabañeros amb isx49189851"
  echo "Usage: prog -f|-d arg1 arg2 arg3 arg4"
  exit 0
fi

# Per cada file comprimir-lo.

files_comprimits=0
STATUS=0

for file in $*
do
  if [ -f $file ]
  then
    gzip $file &> /dev/null
    if [ $? -eq 0 ]
    then
      echo "El fitxer $file s'ha comprimit correctament."
      files_comprimits=$((files_comprimits+1))
    else
      echo "Error! El fitxer $file no s'ha pogut comprimir." >> /dev/stderr
      STATUS=2
    fi
  else
    echo "Error! $file no és un fitxer." >> /dev/stderr
    STATUS=2
  fi

done
echo "S'han comprimit $files_comprimits fitxers"
exit $STATUS
