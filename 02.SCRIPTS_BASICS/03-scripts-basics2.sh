#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció: Processar arguments que són matrícules.
#
#		- Validar que n'hi ha un argument mínim.
#		- Llistar lès vàlides, del tipus: 9999-AAA
#		- Stdout les que són vàlides, per stderr les no vàlides. Retorna de status el número d’errors (de no vàlides).
#	
#	$prog matricula
# ------------------------------

ERR_NOARGS=1

# Validar nº d'arguments

if [ $# -eq 0 ]; then
  echo "ERR: Número args incorrecte"
  echo "Usage: $0 no args"
  exit $ERR_NOARGS
fi

# Llistar les matrícules amb format valid i retorna quantes no ho són.

novalid=0

for matricules in $*
do

  echo $matricules | egrep '^[0-9]{4}-[A-Z]{3}$' 2> /dev/null
  if [ $? -ne 0 ]; then 
    echo "$matricules" >> /dev/stderr 
    novalid=$((novalid+1)) 
  fi

done
exit $novalid

