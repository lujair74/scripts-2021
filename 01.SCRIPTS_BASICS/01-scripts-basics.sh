#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció: Mostrar l’entrada estàndard numerant línia a línia
#	
#	$prog num-stdin
# ------------------------------

# Numera la entrada stdin
num=1
while read -r line
do
  echo "$num. $line"
  ((num++))
done


