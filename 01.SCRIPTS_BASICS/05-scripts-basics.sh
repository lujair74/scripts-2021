#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció: Mostrar línia a línia l’entrada estàndard, retallant només els primers 50 caràcters.
#
#		- Retallar els 50 primers caracters.
#	
#	$prog c50-args
# ------------------------------

# Retallar els 50 primers caràcters entrada stdin.

while read -r line
do
  echo $line |cut -c-50
done

