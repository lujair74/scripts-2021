#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció: Fer un comptador des de zero fins al valor indicat per l’argument rebut.
#
#		- Validar nº d'arguments.
#		- Mostrar el compte enrere.
#	
#	$prog countdown
# ------------------------------

ERR_NOARGS=1

# Validar nº d'arguments

if [ $# -eq 0 ]; then
  echo "ERR: Número args incorrecte"
  echo "Usage: $0 no args"
  exit $ERR_NOARGS
fi

# Mostrar el compte enrere

num=0
MAX="$1"

while [ $num -le $MAX ]
do
  echo "$num"
  ((num++))
done
exit 0


