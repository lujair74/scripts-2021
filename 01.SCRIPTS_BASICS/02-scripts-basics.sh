#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció: Mostar els arguments rebuts línia a línia, tot numerànt-los.
#
#		- Validar nº d'arguments.
#		- Mostrar-los numerats.
#	
#	$prog num-args
# ------------------------------

ERR_NOARGS=1

# Validar nº d'arguments

if [ $# -eq 0 ]; then
  echo "ERR: Número args incorrecte"
  echo "Usage: $0 no args"
  exit $ERR_NOARGS
fi

# Mostrar-los numerats.

num=1

for arg in $*
do
  echo "$num: $arg"
  ((num++))
done
exit 0


