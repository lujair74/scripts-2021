#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció: Fer un programa que rep com a arguments números de més (un o més) i indica per a cada mes rebut quants dies té el més.
#
#		- Validar nº d'arguments.
#		- Validar que el número es valid [[1-12].
#		- Mostrar per mes rebut quants dies te el mes.
#	
#	$prog day-month
# ------------------------------

ERR_NOARGS=1
ERR_NOMONTH=2

# Validar nº d'arguments

if [ $# -eq 0 ]; then
  echo "ERR: Número args incorrecte"
  echo "Usage: $0 no args"
  exit $ERR_NOARGS
fi

# Mostrar el numero de dies per mes introduït

for month in $*
do
  if ! [ $month -ge 1 -a $month -le 12 ]; then
    echo "el $month no és numero de més vàlid."
    echo "Usage: $month [1-12]."
    exit $ERR_NOMONTH
  
  else
    case $month in
      "2")
        echo "El mes $month té 28 dies.";;

      "4"|"6"|"9"|"11")
        echo "El mes $month té 30 dies.";;

      *)
        echo "El mes $month té 31 dies.";;
    esac
  fi

done
exit 0
