#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Exemple if: Validar si es un directori.
#	- Requisits:
#		> Validar si existeix un arg.
#		> Validar si es un directori
#		> Llistar-ho
#	$prog esdir
# ------------------------------

ERR_NARGS=1
ERR_DIR=2

# 1) Validem arguments.

if [ $# -ne 1 ]
then
  echo "Error: número d'arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

# 2) Validar si es un dir.

dir=$1

if ! [ -d $dir ]; then
  echo "Error: $dir no és un directori."
  echo "Usage: $0 dir"
  exit $ERR_DIR
fi

# 3) Llistar-ho

ls $dir
exit 0
