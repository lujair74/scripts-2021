#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció: Crear n directoris
#
#		- Validar nº d'arguments.
#		- No genera errors.
#		- El programa retorna 0 si tots els directoris creats okey.
#		- El programa retorna 1 si hi ha errors en el número d'arguments.
#		- El programa retorna 2 Si algun directori no s'ha pogut crear.
#	
#	$prog args-for
# ------------------------------

ERR_NOARGS=1
ERR_MKDIR=2
STATUS=0

# Validar arguments

if [ $# -eq 0 ]; then
  echo "ERR: Nº d'argument no vàlid."
  exit $ERR_NOARGS
fi

# Per cada argument introduït crear-hi un directori

for dir in $*
do
  mkdir $dir &>/dev/null
  
  if [ $? -ne 0 ]; then
    echo "ERR: no s'ha creat el $dir" >&2
    STATUS=$ERR_MKDIR
  
  else
    STATUS=0
  fi
done

# Mostra el resultat del programa

echo "$STATUS"

