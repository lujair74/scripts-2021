#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció: Exemples bucle while
#	
#	$prog args-for
# ------------------------------

ERR_NOARGS=1

# Validar arguments

if [ $# -eq 0 ]; then
  echo "ERR: Número args incorrecte"
  echo "Usage: $0 [-a -b -c -d -e -r]"
  exit $ERR_NOARGS
fi

# Comprobar quants arguments i opcions n'hi ha

opcions=""
arguments=""

for arg in $*
do
  case $arg in
	  -[abcisr])
		  opcions="$opcions $arg";;
  *)
	  arguments="$arguments $arg"
  esac	  
done

echo "Opcions: $opcions."
echo "Arguments: $arguments."

exit 0
