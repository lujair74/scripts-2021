#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció:
#		- Validar si hi han 2 arguments.
#		- Validar si l'argument primer és un file.
#		- Validar si l'argument segon és un dir.
#		- Per cada element del directori di si es regular, link dir o altres
#
#       $prog copy-files
# ------------------------------  

ERR_NOARGS=1
ERR_NOFILE=2
ERR_NODIR=3

# Validar si hi han 2 arguments.

if [ $# -ne 2 ]; then
  echo "ERR: Número d'argument no vàlid"
  echo "Usage: $0 copyfile."
  exit $ERR_NOARGS
fi

# Validar si el primer argument és un file.

file=$1

if ! [ -f $file ]; then
  echo "ERR: El $file no és un file."
  echo "Usage: $0 file dir-destí."
  exit $ERR_NOFILE
fi

# Validar si l'argument és un directori

dir=$2

if ! [ -d $dir ]; then
  echo "ERR: $dir no és un directori."
  echo "Usage: $0 file dir-destí."
  exit $ERR_NODIR
fi

# Copiar el file al directori destí

cp $file $dir
exit 0
