#! /bin/bash
#
# Exemple de primer programa
# Normes:
#   shebang
#   capçalera: descripció, data, autor
#----------------------------------------
# es pot fer allò que es fa a la línia
# de comandes

echo "Hello World"
nom='pere prou prat'
edat=25

echo $nom $edat
echo -e "$nom\n edat: $edat\n"
echo -e '$nom\n edat: $edat\n'
uname -a
uptime
echo $SHLVL
echo $SHELL
echo $((4*32))
echo $((edat*3))

exit 0
