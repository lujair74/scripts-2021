#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Exemple if: Validar si esta suspès o aprovat
#	- requisits:
#		> Validar si existeix un arg.
#		> Validar argument [0-10]
#	$  prog nota
# ------------------------------

ERR_NARGS=1
ERR_NOTA=2

# 1) Validem arguments.

if [ $# -ne 1 ]
then
  echo "Error: número d'arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi

# 2) Validar argument.

nota=$1

if ! [ $nota -ge 0 -a $nota -le 10 ]
then
  echo "Error: $nota no és un número de nota vàlid."
  echo "nota pren valors de 0 a 10"
  echo "Usage: $0 nota"
  exit $ERR_NOTA
fi

# 3) Validar si està suspés o aprovat.

if [ $nota -lt 5 ]
then
  echo "La nota és: $nota, està suspés."

else
  echo "La nota és: $nota, està aprovat."
fi

