#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció:
#		- Validar si existeix un arg
#		- Validar si l'argument és un directori.
#		- Fer un ls del directori
#		- Per cada element del directori di si es regular, link dir o altres
#
#       $prog exemples-while
# ------------------------------  

ERR_NOARGS=1
ERR_NODIR=2

# Validar si existeix un arg

if [ $# -ne 1 ]; then
  echo "ERR: Número d'argument no vàlid"
  echo "Usage: $0 dir"
  exit $ERR_NOARGS
fi

# Validar si l'argument és un directori

dir=$1

if ! [ -d $dir ]; then
  echo "ERR: $dir no és un directori."
  echo "Usage: $0 dir"
  exit $ERR_NODIR
fi

# Llistar-ho

ls_fitxer=$(ls $dir)

for file in $ls_fitxer
do
  if [ -h "$dir/$file" ]; then
    echo "El fitxer $file és un link."

  elif [ -f "$dir/$file" ]; then
    echo "El fitxer $file és un regular file."

  elif [ -d "$dir/$file" ]; then
    echo "El fitxer $file és un directori."

  else
    echo "El fitxer $file, existeix pero no és cap de les esmentades."
  
  fi
done
exit 0
