#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció:
#		- Validar si existeix un arg
#		- Validar si l'argument és un directori.
#		- Fer un ls del directorit
#
#       $prog exemples-while
# ------------------------------  

ERR_NOARGS=1
ERR_NODIR=2

# Validar si existeix un arg

if [ $# -ne 1 ]; then
  echo "ERR: Número d'argument no vàlid"
  echo "Usage: $0 dir"
  exit $ERR_NOARGS
fi

# Validar si l'argument és un directori

dir=$1

if ! [ -d $dir ]; then
  echo "ERR: $dir no és un directori."
  echo "Usage: $0 dir"
  exit $ERR_NODIR
fi

# Llistar-ho

ls $dir
exit 0
