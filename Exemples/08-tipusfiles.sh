#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Exemple if: Validar el tipus de file
#	- Requisits:
#		> Validar si existeix un arg.
#		> Validar si és un regular file
#		> Validar si és un dir
#		> Validar si és un link
#	$prog esdir
# ------------------------------

ERR_NARGS=1
ERR_FILE=2

# 1) Validem arguments.

if [ $# -ne 1 ]; then
  echo "Error: número d'arguments incorrecte."
  echo "Usage: $0 dir."
  exit $ERR_NARGS
fi

# 2) Validar el tipus de fitxer.

file=$1

if ! [ -e $file ]; then
  echo "Error: $file file no existeix."
  echo "Usage: $0 tipusfile."
  exit $ERR_FILE

elif [ -f $file ]; then
  echo "El fitxer $file és un regular file."

elif [ -h $file ]; then
  echo "El fitxer $file és un link."

elif [ -d $file ]; then
  echo "El fitxer $file és un directori."

else
  echo "El fitxer $file, existeix pero no és cap de les esmentades."
  
fi
exit 0
