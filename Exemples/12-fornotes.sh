#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# EValidar si esta suspès(0-4), aprovat(5-6), notable(7-8) o excel·lent (9-10).
#	- Requisits:
#		> Validar si existeix un arg.
#		> Validar argument [0-10].
#		> Validar la calificació.
#	$prog for nota
# ------------------------------

ERR_NARGS=1

# 1) Validem arguments.

if [ $# -eq 0 ]
then
  echo "Error: número d'arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi

# 2) Validar argument.

notas=$*

for nota in $notas
do
  if ! [ $nota -ge 0 -a $nota -le 10 ]
  then
    echo "Error: $nota no és un número de nota vàlid. [0-10]" >> /dev/stderr
   
# 3) Validar la calificació

  elif [ $nota -lt 5 ]; then
    echo "La nota és: $notas, està suspés."

  elif [ $nota -lt 7 ]; then
    echo "La nota és: $notas, està aprovat."

  elif [ $nota -lt 9 ]; then
    echo "La nota és: $nota, és un notable."

  else
    echo "La nota és: $nota, és un excel·lent."
  fi

done
exit 0

