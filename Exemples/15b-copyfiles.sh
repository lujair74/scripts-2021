#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció:
#		- Validar si hi han 2 arguments.
#		- Validar si l'argument primer és un file.
#		- Validar si l'argument segon és un dir.
#		- Per cada element del directori di si es regular, link dir o altres
#
#       $prog copy-files
# ------------------------------  

ERR_NOARGS=1
ERR_NOFILE=2
ERR_NODIR=3

# Validar si hi han 2 arguments.

if [ $# -lt 2 ]; then
  echo "ERR: Número d'argument no vàlid"
  echo "Usage: $0 copyfile."
  exit $ERR_NOARGS
fi

# Separar destí i llista_files

	# desti=$(echo $* | sed 's/^.* //')
	# llista_files=$(echo $* | sed 's/ [^ ]*$//')

dir=$(echo $* | cut -d' ' -f$#)
llista_files=$(echo $* | cut -d' ' -f1-$(($#-1))

# Validar si l'argument és un directori

if ! [ -d $dir ]; then
  echo "ERR: $dir no és un directori."
  echo "Usage: $0 file dir-destí."
  exit $ERR_NODIR
fi

# Validar si el primer argument és un file.

llista_files=$1

for file in llista_files
do
  if ! [ -f $file ]; then
    echo "ERR: El $file no és un file." 2>>/dev/null

  else;
    cp $file $dir
  fi

done
exit 0

