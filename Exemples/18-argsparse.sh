#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció: Exemples bucle while
#	
#	$prog args-for
# ------------------------------

ERR_NOARGS=1

# Validar arguments

if [ $# -eq 0 ]; then
  echo "ERR: Número args incorrecte"
  echo "Usage: $0 [-a -b -c -d -e -r]"
  exit $ERR_NOARGS
fi

# Comprobar quants arguments i opcions n'hi ha

opcions=""
arguments=""
fitxer=""
num=""

while [ "$1" ]
do
  case "$1" in
  
  -[abcde])
    opcions="$opcions $1";;
  
  "-a")
    opcions="$opcions $1"
    fitxer=$2
    shift;;

  "-d")
    opcions="$opcions $1"
    num=$2
    shift;;

  *)
    arguments="$arguments $1";;
  esac
  shift  

done

echo "Opcions: $opcions."
echo "Arguments: $arguments."
echo "Fitxers: $fitxer"
echo "Num: $num"

exit 0
