#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció: Exemples bucle while
#	
#	$prog exemples-while
# ------------------------------

# 7) Numera l'entrada estandard línia a línia i passar a mayúscula.

num=1

while read -r line
do
  echo "$num: $line" | tr 'a-z' 'A-Z'
  ((num ++))
done
exit 0

# 6) Processa l'entrada estandard fins al token fi.

read -r line
while [ $line != "FI" ]
do
  echo "$line"
  read -r line
done
exit 0

# 5) Processa l'entrada estandard i la mostra numerada línia a línia.

num=1

while read -r line
do
  echo "$num: $line"
  ((num ++))
done
exit 0

# 4) Processar l'entrada estandard línia a línia.

while read -r line
do
  echo $line
done
exit 0

# 3) Iterar arguments amb shift.

while [ -n "$1" ]
do
  echo "$1,$#,$*"
  shift
done
exit 0

# 2) Contador decreixen del arg [N-0].

MIN=0
num=$1

while [ $num -ge $MIN ]
do 
  echo -n "$num", " # -n sirver  para que no haga el salto de línia."
  ((num--))
done
exit 0

# 1) Mostrar un comptador del 1 a MAX.

num=$1
while [ $num -ge 0 ]
do
  echo "$num"
  ((num--))
done
exit 0
