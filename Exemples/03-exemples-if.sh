#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Exemple if: indica si és major d'edat
#	$  prog edat
# ------------------------------

# 1) Validem arguments.

if [ $# -ne 1 ]
then
	echo "Error: número d'arguments incorrecte"
	echo "Usage: $0 edat"
	exit 1
fi

# 2) Validar si és major d'edat.

edat=$1

if [ $edat -ge 18 ]
then
  echo "Edat $edat és major d'edat"
else
  echo "Edat $edat és menor d'edat"
fi
