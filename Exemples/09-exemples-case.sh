#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Exemple case
# ------------------------------

# 3) EXEMPLE 

case $1 in
  "dl"|"dt"|"dc"|"dj"|"dv")
    echo "$1 És un dia laborable";;
   
  "ds"|"dj")
    echo "$1 És un dia festiu";;

  *)
    echo "$1 no és un dia"

esac
exit 0

# 3.1) EXEMPLE

case $1 in
	d[ltcjv])
    echo "$1 És un dia laborable";;
   
  "ds"|"dj")
    echo "$1 És un dia festiu";;

  *)
    echo "$1 no és un dia"

esac
exit 0

# 2) EXEMPLE

case $1 in
  [aeiou])
    echo "$1 és una vocal.";;

  [qwrtypsdfghjklñzxcvbnmç])
    echo "$1 és una consonant.";;

  *)
    echo "$1 és una altra cosa."

esac
exit 0

# 3) EXEMPLE

case $1 in
  "pere"|"pau"|"joan")
    echo "és un nen.";;

  "marta"|"anna"|"julia")
    echo "és una nena.";;

  *)
    echo "és indefini"

esac
exit 0
