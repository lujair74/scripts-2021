#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Exemple if: Validar si es un directori.
#	- Requisits:
#		> Validar si existeix un arg.
#		> Validar si s'ha cridat la ayuda
#		> Validar si es un directori
#	$prog esdir
# ------------------------------

ERR_NARGS=1
ERR_DIR=2

# 1) Validem arguments.

if [ $# -ne 1 ]
then
  echo "Error: número d'arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

# 2) Validar si ha cridat -h.

if [ "$1" = "-h" -o "$1" = "--help" ]; then
  echo "Programa: $0 dir"
  echo "Author: @edt"
  echo "That's all folks"
  exit 0
fi

# 3) Si no és un directori mostrar ERROR.

if ! [ -d
