#! /bin/bash
# @edt ASIX M01-ISO
# Febrer 2022
# Descripció:
#		- Validar si existeix un arg
#		- Validar si l'argument és un directori.
#		- Fer un ls del directori
#		- Per cada element del directori di si es regular, link dir o altres
#
#       $prog exemples-while
# ------------------------------  

ERR_NOARGS=1
ERR_NODIR=2

# Validar si existeix un arg

if [ $# -eq 0 ]; then
  echo "ERR: Número d'argument no vàlid"
  echo "Usage: $0 dir"
  exit $ERR_NOARGS
fi

# Validar si l'argument és un directori

llista_dir=$*

for dir in $llista_dir
do
  if ! [ -d $dir ]; then
    echo "ERR: $dir no és un directori." 2>>/dev/null
  fi
done

# Llistar-ho

ls_fitxer=$(ls $llista_dir)

for file in $ls_fitxer
do
  if [ -h "$llista_dir/$file" ]; then
    echo "El fitxer $file és un link."

  elif [ -f "$llista_dir/$file" ]; then
    echo "El fitxer $file és un regular file."

  elif [ -d "$llista_dir/$file" ]; then
    echo "El fitxer $file és un directori."

  else
    echo "El fitxer $file, existeix pero no és cap de les esmentades."
  
  fi
done
exit 0
